"""This module contains test harness elements common to all unit tests."""
import collections
import logging
from typing import Dict

import pytest
from ska_tango_base.testing.mock import MockCallable, MockChangeEventCallback


@pytest.fixture()
def logger() -> logging.Logger:
    """
    Fixture that returns a default logger.

    :return: a logger
    """
    return logging.getLogger()


@pytest.fixture(name="callbacks")
def fixture_callbacks() -> Dict[str, MockCallable]:
    """
    Return a dictionary of callbacks with asynchrony support.

    :return: a collections.defaultdict that returns callbacks by name.
    """
    return collections.defaultdict(MockCallable)


@pytest.fixture(name="communication_status_callback")
def fixture_communication_status_callback(
    callbacks: Dict[str, MockCallable]
) -> MockCallable:
    """
    Return a mock callback with asynchrony support.

    This mock callback can be registered with the component manager, so
    that it is called when the status of communication with the
    component changes.

    :param callbacks: a dictionary from which callbacks with asynchrony
        support can be accessed.

    :return: a callback with asynchrony support
    """
    return callbacks["communication_status"]


@pytest.fixture(name="component_state_callback")
def fixture_component_state_callback(
    callbacks: Dict[str, MockCallable]
) -> MockCallable:
    """
    Return a mock callback with asynchrony support.

    This mock callback can be registered with the component manager, so
    that it is called when the state of the component changes.

    :param callbacks: a dictionary from which callbacks with asynchrony
        support can be accessed.

    :return: a callback with asynchrony support
    """
    return callbacks["component_state"]


@pytest.fixture()
def change_event_callbacks() -> Dict[str, MockChangeEventCallback]:
    """
    Return a dictionary of change event callbacks with asynchrony support.

    :return: a collections.defaultdict that returns change event
        callbacks by name.
    """

    class _ChangeEventDict(dict):
        def __missing__(self, key: str) -> MockChangeEventCallback:
            self[key] = MockChangeEventCallback(key)
            return self[key]

    return _ChangeEventDict()

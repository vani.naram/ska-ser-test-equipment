"""Tests of the SignalGeneratorComponentManager."""
import pytest
from ska_tango_base.control_model import CommunicationStatus, PowerState
from ska_tango_base.testing.mock import MockCallable

from ska_ser_test_equipment.scpi_over_tcp_simulator import ScpiOverTcpSimulator
from ska_ser_test_equipment.signal_generator import (
    SignalGeneratorComponentManager,
)


def test_monitoring(
    signal_generator_simulator: ScpiOverTcpSimulator,
    signal_generator_component_manager: SignalGeneratorComponentManager,
    communication_status_callback: MockCallable,
    component_state_callback: MockCallable,
) -> None:
    """
    Test the component manager's monitoring of the signal processor.

    :param signal_generator_simulator: the signal generator simulator
        that the component manager under test connects to
    :param signal_generator_component_manager: the component manager
        under test
    :param communication_status_callback: a callback that has already
        been passed to the component manager, to be called when the
        status of communication with the processor changes
    :param component_state_callback: a callback that has already been
        passed to the component manager, to be called when the state of
        the processor changes.
    """
    assert (
        signal_generator_component_manager.communication_state
        == CommunicationStatus.DISABLED
    )
    assert (
        signal_generator_component_manager.component_state["power"]
        == PowerState.UNKNOWN
    )
    assert signal_generator_component_manager.component_state["fault"] is None

    signal_generator_component_manager.start_communicating()

    communication_status_callback.assert_next_call(
        CommunicationStatus.NOT_ESTABLISHED
    )
    communication_status_callback.assert_next_call(
        CommunicationStatus.ESTABLISHED
    )

    component_state_callback.assert_next_call(
        rf_output_on=True,
        power_dbm=1.0,
        frequency=0.1,
        am_modulation_on=True,
        fm_modulation_on=False,
        pm_modulation_on=False,
        lfo_sweep_mode="MAN",
        power=PowerState.ON,
        fault=False,
    )

    # The values in the simulator are static, so we won't change unless
    # we change them ourselves.
    component_state_callback.assert_not_called()

    # Now let's change a value in the simulator, and check that the
    # component manager detects and reports it
    signal_generator_simulator.write_field_value(b"PM:STAT", b"1")
    component_state_callback.assert_next_call(pm_modulation_on=True)

    signal_generator_component_manager.stop_communicating()

    communication_status_callback.assert_next_call(
        CommunicationStatus.DISABLED
    )
    assert (
        signal_generator_component_manager.communication_state
        == CommunicationStatus.DISABLED
    )
    assert (
        signal_generator_component_manager.component_state["power"]
        == PowerState.UNKNOWN
    )
    assert signal_generator_component_manager.component_state["fault"] is None


def test_write(
    signal_generator_simulator: ScpiOverTcpSimulator,
    signal_generator_component_manager: SignalGeneratorComponentManager,
    communication_status_callback: MockCallable,
    component_state_callback: MockCallable,
) -> None:
    """
    Test the ability to write new values to the signal generator.

    :param signal_generator_simulator: the signal generator simulator
        that the component manager under test connects to
    :param signal_generator_component_manager: the component manager under test
    :param communication_status_callback: a callback that has already
        been passed to the component manager, to be called when the
        status of communication with the processor changes
    :param component_state_callback: a callback that has already been
        passed to the component manager, to be called when the state of
        the processor changes.
    """
    signal_generator_component_manager.start_communicating()
    communication_status_callback.assert_next_call(
        CommunicationStatus.NOT_ESTABLISHED
    )
    communication_status_callback.assert_next_call(
        CommunicationStatus.ESTABLISHED
    )

    component_state_callback.assert_next_call(
        rf_output_on=True,
        power_dbm=1.0,
        frequency=0.1,
        am_modulation_on=True,
        fm_modulation_on=False,
        pm_modulation_on=False,
        lfo_sweep_mode="MAN",
        power=PowerState.ON,
        fault=False,
    )

    signal_generator_component_manager.write_field(
        power_dbm=2.0,
        fm_modulation_on=True,
        lfo_sweep_mode="STEP",
    )

    component_state_callback.assert_next_call(
        power_dbm=2.0,
        fm_modulation_on=True,
        lfo_sweep_mode="STEP",
    )

    field_value = signal_generator_simulator.read_field_value(b"POW")
    assert float(field_value) == pytest.approx(2.0)

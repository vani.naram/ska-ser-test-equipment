"""test of Alveo device using DeviceTestContext."""
from typing import Any, Dict

import pytest
import tango
from ska_tango_base.control_model import AdminMode
from ska_tango_base.testing.mock import MockChangeEventCallback

from ska_ser_test_equipment.signal_generator import SignalGeneratorDevice
from ska_ser_test_equipment.signal_generator.signal_generator_data import (
    signal_generator_interface_definition,
)


@pytest.mark.forked
def test_init(signal_generator_device: SignalGeneratorDevice) -> None:
    """
    Test device initialisation.

    :param signal_generator_device: the signal generator Tango device
        under test.
    """
    assert signal_generator_device.adminMode == AdminMode.OFFLINE
    assert signal_generator_device.state() == tango.DevState.DISABLE

    device_attribute_list = signal_generator_device.get_attribute_list()
    for attr_name in signal_generator_interface_definition:
        assert attr_name in device_attribute_list


@pytest.mark.forked
def test_monitor_and_control(
    signal_generator_device: SignalGeneratorDevice,
    change_event_callbacks: Dict[str, MockChangeEventCallback],
    expected_attribute_values: Dict[str, Any],
) -> None:
    """
    Test device's monitoring of the signal generator.

    :param signal_generator_device: the signal generator Tango device
        under test.
    :param change_event_callbacks: dictionary of mock change event
        callbacks with asynchrony support
    :param expected_attribute_values: dictioanry of attribute values
        that we expect
    """
    assert signal_generator_device.adminMode == AdminMode.OFFLINE
    assert signal_generator_device.state() == tango.DevState.DISABLE

    signal_generator_device.subscribe_event(
        "adminMode",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["adminMode"],
    )
    change_event_callbacks["adminMode"].assert_next_change_event(
        AdminMode.OFFLINE
    )

    signal_generator_device.subscribe_event(
        "state",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["state"],
    )
    change_event_callbacks["state"].assert_next_change_event(
        tango.DevState.DISABLE
    )

    signal_generator_device.adminMode = AdminMode.ONLINE

    change_event_callbacks["adminMode"].assert_next_change_event(
        AdminMode.ONLINE
    )

    assert signal_generator_device.adminMode == AdminMode.ONLINE

    change_event_callbacks["state"].assert_next_change_event(
        tango.DevState.UNKNOWN
    )
    change_event_callbacks["state"].assert_next_change_event(tango.DevState.ON)

    device_attribute_list = signal_generator_device.get_attribute_list()
    for attribute_name in signal_generator_interface_definition:
        assert attribute_name in device_attribute_list

        signal_generator_device.subscribe_event(
            attribute_name,
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks[attribute_name],
        )

    for attribute_name in signal_generator_interface_definition:
        change_event_callbacks[attribute_name].assert_next_change_event(
            expected_attribute_values[attribute_name]
        )
        assert (
            getattr(signal_generator_device, attribute_name)
            == expected_attribute_values[attribute_name]
        )

    # Now let's test we can control the signal generator, by changing a value.
    signal_generator_device.power_dbm = 2.0
    change_event_callbacks["power_dbm"].assert_next_change_event(
        pytest.approx(2.0)
    )
    assert signal_generator_device.power_dbm == pytest.approx(2.0)

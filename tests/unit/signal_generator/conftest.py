"""Test harness for signal generator tests."""
import logging
import threading
import time
from typing import Any, Callable, Dict, Generator, Tuple

import pytest
import tango
import tango.test_context

from ska_ser_test_equipment.scpi_over_tcp_simulator import ScpiOverTcpSimulator
from ska_ser_test_equipment.signal_generator import (
    SignalGeneratorComponentManager,
    SignalGeneratorDevice,
)


@pytest.fixture(name="field_values")
def field_values_fixture() -> Dict[bytes, bytes]:
    """
    Return values that the signal generator socket simulator should take.

    :return: signal generator socket simulator field values.
    """
    # TODO: This has to be manually kept in sync with the
    # expected_attribute_values fixture. We need a better way to do this.
    return {
        b"OUTP1": b"1",
        b"POW": b"1.0",
        b"FREQ": b"100000",
        b"AM:STAT": b"1",
        b"FM:STAT": b"0",
        b"PM:STAT": b"0",
        b"LFO:SWE:MODE": b"MAN",
    }


@pytest.fixture(name="expected_attribute_values")
def expected_attribute_values_fixture() -> Dict[str, Any]:
    """
    Return a dictionary of expected signal generator device attribute values.

    :return: expected signal generator device attribute values.
    """
    # TODO: This has to be manually kept in sync with the field_values fixture.
    # We need a better way to do this.
    return {
        "rf_output_on": True,
        "power_dbm": 1.0,
        "frequency": 0.1,
        "am_modulation_on": True,
        "fm_modulation_on": False,
        "pm_modulation_on": False,
        "lfo_sweep_mode": "MAN",
    }


@pytest.fixture()
def signal_generator_simulator(
    field_values: Dict[bytes, bytes]
) -> Generator[ScpiOverTcpSimulator, None, None]:
    """
    Return a signal generator simulator for use in testing.

    The simulator will already be running as a TCP server on a separate
    thread.

    :param field_values: dictionary of field values that the simulator
        should be initialised to take

    :yields: a signal generator simulator
    """
    address = ("localhost", 0)  # let the kernel give us a port
    server = ScpiOverTcpSimulator(address, field_values)

    t = threading.Thread(target=server.serve_forever)
    t.setDaemon(True)  # don't hang on exit
    t.start()

    yield server
    server.shutdown()


@pytest.fixture()
def signal_generator_address(
    signal_generator_simulator: ScpiOverTcpSimulator,
) -> Tuple[str, int]:
    """
    Return the address of the signal generator.

    :param signal_generator_simulator: a signal generator simulator,
        already running as a TCP server.

    :return: the address of the signal generator
    """
    return signal_generator_simulator.server_address


@pytest.fixture()
def signal_generator_component_manager(
    signal_generator_address: Tuple[str, int],
    logger: logging.Logger,
    communication_status_callback: Callable,
    component_state_callback: Callable,
) -> SignalGeneratorComponentManager:
    """
    Return the signal generator component manager under test.

    :param signal_generator_address: host and port of the signal
        generator
    :param logger: a logger for the component manager to use.
    :param communication_status_callback: a callback to be registered
        with the component manager, to be called when the status of
        communication with the Alveo changes
    :param component_state_callback: a callback to be registered with
        the component manager, to be called when the state of the component
        changes.

    :return: a signal generator component manager
    """
    return SignalGeneratorComponentManager(
        signal_generator_address[0],
        signal_generator_address[1],
        logger,
        communication_status_callback,
        component_state_callback,
        poll_rate=0.5,  # speed it up for testing purposes
    )


@pytest.fixture()
def signal_generator_device(
    signal_generator_address: Tuple[str, int],
) -> Generator[tango.DeviceProxy, None, None]:
    """
    Yield a proxy to the signal generator, running in a test context.

    :param signal_generator_address: host and port of the signal
        generator

    :yields: a proxy to the device under test
    """
    context = tango.test_context.DeviceTestContext(
        SignalGeneratorDevice,
        properties={
            "Host": signal_generator_address[0],
            "Port": signal_generator_address[1],
            "PollRate": 0.5,
        },
        daemon=True,
    )
    context.start()
    time.sleep(0.15)
    yield context.device
    context.stop()

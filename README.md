# ska-ser-test-equipment

This project provides Tango device access to test equipment hardware
within the [Square Kilometre Array](https://skatelescope.org/).

Documentation
-------------

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-ser-test-equipment/badge/?version=latest)](https://developer.skao.int/projects/ska-ser-test-equipment/en/master/?badge=latest)

The documentation for this project, including how to get started with it, can be found in the `docs` folder, or browsed in the SKA development portal:

* [ska-ser-test-equipment documentation](https://developer.skatelescope.org/projects/ska-ser-test-equipment/en/latest/index.html "SKA Developer Portal: ska-ser-test-equipment documentation")

# -*- coding: utf-8 -*-
#
# (c) 2022 CSIRO.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""This module provides a general framework for polling."""
# TODO: This entire module should be considered for inclusion in
# ska-tango-base.

import threading
from typing import Any, Dict

from typing_extensions import Protocol


# pylint: disable=no-self-use, unused-argument
class PollModelProtocol(Protocol):
    """Protocol for a class that the poller can poll."""

    def poll(self, values_to_write: Dict[str, Any]) -> Dict[str, Any]:
        """
        Perform a single poll.

        During the poll, the provided values should be written. All
        available values should be read and returned.

        :param values_to_write: values to write during the poll.

        :return: values read during the poll.
        """  # noqa: DAR202
        ...

    def polling_started(self) -> None:
        """
        Respond to polling having started.

        This is a hook called by the poller when it starts polling.
        """
        ...

    def polling_stopped(self) -> None:
        """
        Respond to polling having stopped.

        This is a hook called by the poller when it stops polling.
        """
        ...

    def polling_exception(self, exception: Exception) -> None:
        """
        Respond to an exception being raised by a poll attempt.

        This is a hook called by the poller when an exception occurs.
        The polling loop itself never raises exceptions. It catches
        everything and simply calls this hook to let the polling model
        know what it caught.

        :param exception: the exception that was raised by a recent poll
            attempt.
        """
        ...

    def polling_values(self, **values: Any) -> None:
        """
        Handle the receipt of new polling values.

        This is a hook called by the poller when values have been read
        during a poll.

        :param values: values read during the most recent poll.
        """
        ...


class Poller:
    """A generic hardware polling mechanism."""

    def __init__(
        self, poll_model: PollModelProtocol, poll_rate: float = 1.0
    ) -> None:
        """
        Initialise a new instance.

        :param poll_model: an object that this poller will call to both
            execute polls and provide with results
        :param poll_rate: how long (in seconds) to wait after polling,
            before polling again
        """
        self._poll_model = poll_model
        self._poll_rate = poll_rate

        self._write_values: Dict[str, Any] = {}
        self._write_lock = threading.Lock()

        self._start_polling_event = threading.Event()
        self._stop_polling_event = threading.Event()

        self._polling_thread = threading.Thread(
            target=self._polling_loop,
            daemon=True,
        )

        # doesn't start polling, only starts the polling thread!
        self._polling_thread.start()

    def start_polling(self) -> None:
        """Start polling."""
        self._start_polling_event.set()

    def stop_polling(self) -> None:
        """Stop polling."""
        self._stop_polling_event.set()

    def write(self, **kwargs: Any) -> None:
        """
        Update values to be written on the next poll.

        :param kwargs: keyword arguments specifying fields to be
            written.
        """
        with self._write_lock:
            self._write_values.update(kwargs)

    def _get_write_values(self) -> Dict[str, Any]:
        """
        Get any values to be written.

        :return: a copy of the values to be written.
        """
        with self._write_lock:
            write_values = self._write_values.copy()
            self._write_values.clear()
            return write_values

    def _polling_loop(self) -> None:
        """Loop forever, either polling the hardware, or waiting to do so."""
        while True:
            # block on "start" event
            self._start_polling_event.wait()

            # "start" event received; update state then poll until "stop" event
            # received.
            self._stop_polling_event.clear()
            self._poll_model.polling_started()
            while not self._stop_polling_event.is_set():
                write_values = self._get_write_values()
                try:
                    read_values = self._poll_model.poll(write_values)
                except Exception as exception:  # pylint: disable=broad-except
                    # Deliberately catching all exceptions, because
                    # we're on a thread, and an uncaught exception will
                    # silently take the thread down.
                    self._poll_model.polling_exception(exception)
                else:
                    self._poll_model.polling_values(**read_values)

                self._stop_polling_event.wait(self._poll_rate)

            # "stop" event received; update state, then back to top of
            # loop i.e. block on "start" event
            self._poll_model.polling_stopped()
            self._start_polling_event.clear()

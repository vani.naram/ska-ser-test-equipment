"""This module provides a simulator for a signal generator."""
import socketserver
from typing import Dict, Tuple, cast


class ScpiOverTcpSimulatorRequestHandler(socketserver.BaseRequestHandler):
    """Request handler for a SCPI-over-TCP simulator."""

    def handle(self) -> None:
        """Handle a SCPI-formatted client request."""
        data = self.request.recv(1024)

        commands = [command.strip() for command in data.split(b";")]

        responses = []
        for command in commands:
            if command.endswith(b"?"):
                field = command[:-1]
                value = cast(ScpiOverTcpSimulator, self.server).field_values[
                    field
                ]
                responses.append(value)
            else:
                (field, value) = command.split()
                cast(ScpiOverTcpSimulator, self.server).field_values[
                    field
                ] = value
        response = b";".join(responses) + b"\r\n"
        self.request.sendall(response)


class ScpiOverTcpSimulator(socketserver.TCPServer):
    """A simple simulator of hardware that talks SCPI over TCP."""

    def __init__(
        self, server_address: Tuple[str, int], field_values: Dict[bytes, bytes]
    ) -> None:
        """
        Initialise a new instance.

        :param server_address: address on which to run the server. Note
            that this is not necessarily the address at which the client
            will connect: we might provide an empty host or port here,
            allowing the server to bind to any interface / port.
        :param field_values: a dictionary of initial values for the
            simulator to take.
        """
        self.field_values = field_values
        super().__init__(server_address, ScpiOverTcpSimulatorRequestHandler)

    def write_field_value(self, field_name: bytes, field_value: bytes) -> None:
        """
        Write a field value to the simulator.

        :param field_name: name of the field to be updated (in bytes)
        :param field_value: new value for the field (in bytes).
        """
        self.field_values[field_name] = field_value

    def read_field_value(self, field_name: bytes) -> bytes:
        """
        Read a field value from the simulator.

        :param field_name: name of the field to be read (in bytes).

        :return: value of the field (in bytes).
        """
        return self.field_values[field_name]

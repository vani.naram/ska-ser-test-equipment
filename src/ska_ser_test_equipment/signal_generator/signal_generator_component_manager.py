# -*- coding: utf-8 -*-
#
# (c) 2022 CSIRO.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""This module provides for monitoring and control of a signal generator."""
import logging
import socket
from contextlib import closing
from typing import Any, Callable, Dict, NoReturn

from ska_tango_base.base import BaseComponentManager
from ska_tango_base.control_model import CommunicationStatus, PowerState

from ska_ser_test_equipment.poller import Poller, PollModelProtocol

from .signal_generator_data import signal_generator_interface_definition


class SignalGeneratorComponentManager(BaseComponentManager, PollModelProtocol):
    """A component manager for a signal generator."""

    def __init__(  # pylint: disable=too-many-arguments
        self,
        host: str,
        port: int,
        logger: logging.Logger,
        communication_state_callback: Callable,
        component_state_callback: Callable,
        poll_rate: float = 1.0,
        timeout: float = 1.0,
    ) -> None:
        """
        Initialise a new instance.

        :param host: the host name or IP address of the signal
            generator.
        :param port: the port of the signal generator.
        :param logger: a logger for this component manager to use for
            logging
        :param communication_state_callback: callback to be called when
            the status of communications between the component manager
            and its component changes.
        :param component_state_callback: callback to be called when the
            state of the component changes.
        :param poll_rate: how long (in seconds) to wait after polling,
            before polling again
        :param timeout: how long (in seconds) to wait after trying to
            connect to the signal generator, before giving up.
        """
        self._host = host
        self._port = port

        self._timeout = timeout

        self._poller = Poller(self, poll_rate)

        super().__init__(
            logger,
            communication_state_callback,
            component_state_callback,
            power=PowerState.UNKNOWN,
            fault=None,
            rf_output_on=None,
            power_dbm=None,
            frequency=None,
            am_modulation_on=None,
            fm_modulation_on=None,
            pm_modulation_on=None,
            lfo_sweep_mode=None,
        )

    def start_communicating(self) -> None:
        """Start polling the signal generator."""
        if self.communication_state == CommunicationStatus.ESTABLISHED:
            return
        if self.communication_state == CommunicationStatus.DISABLED:
            self._update_communication_state(
                CommunicationStatus.NOT_ESTABLISHED
            )
        # and we remain in NOT_ESTABLISHED until the polling loop is
        # actually talking to the signal generator. It will tell us this
        # is the case by calling our `polling_started` method.

        self._poller.start_polling()

    def polling_started(self) -> None:
        """
        Respond to polling having started.

        This is a hook called by the poller when it starts polling.
        """
        # There's no need to do anything here. We wait to receive some polled
        # values before we declare communication to be established.

    def stop_communicating(self) -> None:
        """Stop polling the signal generator."""
        if self.communication_state == CommunicationStatus.DISABLED:
            return
        # communication remains ESTABLISHED until the polling loop
        # actually stops polling. It will tell us that it has stopped by
        # calling the `polling_stopped` method.

        self._poller.stop_polling()

    def polling_stopped(self) -> None:
        """
        Respond to polling having stopped.

        This is a hook called by the poller when it stops polling.
        """
        self._update_component_state(power=PowerState.UNKNOWN, fault=None)
        self._update_communication_state(CommunicationStatus.DISABLED)

    def off(self, task_callback: Callable) -> NoReturn:
        """
        Turn the component off.

        :param task_callback: callback to be called when the status of
            the command changes

        :raises NotImplementedError: because this command is not yet
            implemented
        """
        raise NotImplementedError(
            "The signal generator cannot be turned off or on right now."
        )

    def standby(self, task_callback: Callable) -> NoReturn:
        """
        Put the component into low-power standby mode.

        :param task_callback: callback to be called when the status of
            the command changes

        :raises NotImplementedError: because this command is not yet
            implemented
        """
        raise NotImplementedError(
            "The signal generator cannot be put into standby mode."
        )

    def on(
        self, task_callback: Callable
    ) -> NoReturn:  # pylint: disable=invalid-name
        """
        Turn the component on.

        :param task_callback: callback to be called when the status of
            the command changes

        :raises NotImplementedError: because this command is not yet
            implemented
        """
        raise NotImplementedError(
            "The signal generator cannot be turned off or on right now."
        )

    def reset(self, task_callback: Callable) -> NoReturn:
        """
        Reset the component (from fault state).

        :param task_callback: callback to be called when the status of
            the command changes

        :raises NotImplementedError: because this command is not yet
            implemented
        """
        # TODO: Presumably this would be implemented to send "*RST" command.
        raise NotImplementedError(
            "The signal generator cannot (yet) reset its component."
        )

    def write_field(self, **kwargs: Any) -> None:
        """
        Update signal generator field value(s).

        This doesn't actually write to the signal generator. It simply
        stores the details of the requested write where it will be
        picked up by the next iteration of the polling loop.

        :param kwargs: keyword arguments specifying fields to be
            written.
        """
        self._poller.write(**kwargs)

    def polling_exception(self, exception: Exception) -> None:
        """
        Respond to an exception being raised by a poll attempt.

        This is a hook called by the poller when an exception occurs.

        :param exception: the exception that was raised by a recent poll
            attempt.
        """
        self._update_communication_state(CommunicationStatus.NOT_ESTABLISHED)
        self.logger.error(f"Poll failed: {repr(exception)}")

    def polling_values(self, **values: Any) -> None:
        """
        Handle the receipt of new polling values.

        This is a hook called by the poller when values have been read
        during a poll.

        :param values: values read during the most recent poll.
        """
        # Reiterate that communication is established, just in case it
        # had dropped out.
        self._update_communication_state(CommunicationStatus.ESTABLISHED)

        # Evaluate the values we have just read, and decide whether the
        # device is in fault. For now we decide everything is okay.
        fault = False

        # Always-on device
        self._update_component_state(
            power=PowerState.ON, fault=fault, **values
        )

    def poll(self, values_to_write: Dict[str, Any]) -> Dict[str, Any]:
        """
        Poll the hardware.

        Connect to the hardware, write any values that are to be
        written, and then read all values.

        :param values_to_write: any field values that should be written
            to the hardware on this poll.

        :return: updated field values from the hardware
        """
        commands = []
        for (
            attribute,
            interface_spec,
        ) in signal_generator_interface_definition.items():
            field_name = interface_spec["field"]

            if attribute in values_to_write:
                attribute_value = values_to_write[attribute]
                if interface_spec["type"] == "float":
                    field_value = str(
                        attribute_value * interface_spec.get("scale", 1.0)
                    )
                elif interface_spec["type"] == "bool":
                    field_value = "1" if attribute_value else "0"
                else:
                    field_value = attribute_value
                commands.append(f"{field_name} {field_value}")
            commands.append(f"{field_name}?")
        command_string = ";".join(commands)

        with closing(
            socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        ) as poll_socket:
            poll_socket.connect((self._host, self._port))
            poll_socket.settimeout(self._timeout)
            poll_socket.sendall(command_string.encode())
            data = poll_socket.recv(1024)

        responses = [
            response.strip() for response in data.decode("utf-8").split(";")
        ]

        read_values = {}
        for (attribute, interface_spec), field_value in zip(
            signal_generator_interface_definition.items(), responses
        ):
            if interface_spec["type"] == "float":
                attribute_value = float(field_value) / interface_spec.get(
                    "scale", 1.0
                )
            elif interface_spec["type"] == "bool":
                attribute_value = field_value == "1"
            else:
                attribute_value = field_value
            read_values[attribute] = attribute_value

        return read_values

# -*- coding: utf-8 -*-
#
# (c) 2022 CSIRO.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""This module provides a Tango device for a signal generator."""
from typing import Any, Dict, Optional, Tuple

import tango
import tango.server
from ska_tango_base import SKABaseDevice
from ska_tango_base.commands import DeviceInitCommand, ResultCode
from ska_tango_base.control_model import PowerState

from ska_ser_test_equipment import __version__

from .signal_generator_component_manager import SignalGeneratorComponentManager
from .signal_generator_data import signal_generator_interface_definition

__all__ = ["SignalGeneratorDevice", "main"]


class SignalGeneratorDevice(SKABaseDevice):
    """A Tango device for monitor and control of a signal generator."""

    # ----------
    # Properties
    # ----------
    Host = tango.server.device_property(dtype=str)
    Port = tango.server.device_property(dtype=int)
    PollRate = tango.server.device_property(dtype=float, default_value=1.0)
    PollTimeout = tango.server.device_property(dtype=float, default_value=1.0)

    # --------------
    # Initialization
    # --------------
    def create_component_manager(self) -> SignalGeneratorComponentManager:
        """
        Create and return a component manager for this device.

        :return: a component manager for this device.
        """
        return SignalGeneratorComponentManager(
            self.Host,
            self.Port,
            self.logger,
            self._communication_state_changed,
            self._component_state_changed,
            poll_rate=self.PollRate,
            timeout=self.PollTimeout,
        )

    class InitCommand(
        DeviceInitCommand
    ):  # pylint: disable=too-few-public-methods
        """Initialisation command class for this AlveoDevice."""

        # pylint: disable=arguments-differ, protected-access
        def do(self) -> Tuple[ResultCode, str]:
            """
            Initialise the attributes of this SignalGeneratorDevice.

            :return: a resultcode, message tuple
            """
            self._device._version_id = __version__
            self._device._setup_signal_generator_attributes()

            message = "SignalGeneratorDevice init complete."
            self._device.logger.info(message)
            self._completed()
            return (ResultCode.OK, message)

    # ----------
    # Attributes
    # ----------
    def _setup_signal_generator_attributes(self) -> None:
        """Set up attributes for the signal generator."""
        # pylint: disable-next=attribute-defined-outside-init
        self._signal_generator_state: Dict[str, Any] = {}

        for name, data in signal_generator_interface_definition.items():
            self._signal_generator_state[name] = None

            attr = tango.server.attribute(
                name=name,
                dtype=data["type"],
                access=tango.AttrWriteType.READ_WRITE,
                label=name,
                max_dim_x=1,
                fread="_read_signal_generator_attribute",
                fwrite="_write_signal_generator_attribute",
            ).to_attr()

            self.add_attribute(
                attr,
                self._read_signal_generator_attribute,
                self._write_signal_generator_attribute,
                None,
            )

            self.set_change_event(name, True, False)
            # TODO: That last False means we push every time, without checking
            # how much the attribute has changed. This is silly for float
            # attributes like power_dbm. We should support abs/rel change on
            # attributes, and then turn on the Tango's built-in change
            # detection.

    def _read_signal_generator_attribute(
        self, attribute: tango.Attribute
    ) -> None:
        name = attribute.get_name()
        value = self._signal_generator_state[name]
        attribute.set_value(value)

    def _write_signal_generator_attribute(
        self, attribute: tango.Attribute
    ) -> None:
        name = attribute.get_name()
        value = attribute.get_write_value()
        self.component_manager.write_field(**{name: value})

    # ----------
    # Callbacks
    # ----------
    def _component_state_changed(
        self,
        fault: Optional[bool] = None,
        power: Optional[PowerState] = None,
        **kwargs: Any,
    ) -> None:
        super()._component_state_changed(fault=fault, power=power)

        for (name, value) in kwargs.items():
            if self._signal_generator_state[name] != value:
                self._signal_generator_state[name] = value
                self.push_change_event(name, value)


# ----------
# Run server
# ----------
def main(args: Any = None, **kwargs: Any) -> int:
    """
    Launch a `SignalGeneratorDevice` server instance.

    :param args: arguments to the device.
    :param kwargs: keyword arguments to the server

    :returns: the Tango server exit code
    """
    return tango.server.run((SignalGeneratorDevice,), args=args, **kwargs)


if __name__ == "__main__":
    main()

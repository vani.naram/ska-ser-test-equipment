# -*- coding: utf-8 -*-
#
# (c) 2022 CSIRO.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""This module contains information about the signal generator."""

from typing import Dict

from typing_extensions import TypedDict

FieldSpec = TypedDict(
    "FieldSpec",
    {
        "field": str,
        "type": str,
        "scale": float,
    },
    total=False,
)


signal_generator_interface_definition: Dict[str, FieldSpec] = {
    "rf_output_on": {
        "field": "OUTP1",
        "type": "bool",
    },
    "power_dbm": {
        "field": "POW",
        "type": "float",
    },
    "frequency": {
        "field": "FREQ",
        "type": "float",
        "scale": 1e6,
    },
    "am_modulation_on": {
        "field": "AM:STAT",
        "type": "bool",
    },
    "fm_modulation_on": {
        "field": "FM:STAT",
        "type": "bool",
    },
    "pm_modulation_on": {
        "field": "PM:STAT",
        "type": "bool",
    },
    "lfo_sweep_mode": {
        "field": "LFO:SWE:MODE",
        "type": "str",
    },
}
